﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace code_challenge.Challenges
{
    public class ScrabbleScoreCalculator
    {

        /*
         * Given the below scoring list create a Scrabble word calculator that will provide the correct scores dependent on the string provided.
         *         
         * Letter                             Value
         * A, E, I, O, U, L, N, R, S, T       1
         * D, G                               2
         * B, C, M, P                         3
         * F, H, V, W, Y                      4
         * K                                  5
         * J, X                               8
         * Q, Z                               10
         */

        // The character arrays have already been set up for you below
        private char[] OnePointValues = { 'a', 'e', 'i', 'o', 'u', 'l', 'n', 'r', 's', 't' };
        private char[] TwoPointValues = { 'd', 'g' };
        private char[] ThreePointValues = { 'b', 'c', 'm', 'p' };
        private char[] FourPointValues = { 'f', 'h', 'v', 'w', 'y' };
        private char[] FivePointValues = { 'k' };
        private char[] EightPointValues = { 'j', 'x' };
        private char[] TenPointValues = { 'q', 'z' };

        // uncomment the method below that will accept a string argument and expect an int to be returned

        public int WordScoreCheck(string word)
        {
            int wordScore = 0;
            bool charFound;
            string lowerWord = word.ToLower();
            for(int i = 0; i < lowerWord.Length; i++)
            {
                charFound = false;
                //Console.WriteLine("Character checked: " + word[i]);
                while(!charFound)
                {
                    for (int j = 0; j < OnePointValues.Length; j++)
                    {
                        if (lowerWord[i] == OnePointValues[j])
                        {
                            //Console.WriteLine(word[i] + " checking for 1 point");
                            wordScore += 1;
                            charFound = true;
                            break;
                            
                        }

                    }
                    for (int j = 0; j < TwoPointValues.Length; j++)
                    {
                        if (lowerWord[i] == TwoPointValues[j])
                        {
                            //Console.WriteLine(word[i] + " checking for 2 points");
                            wordScore += 2;
                            charFound = true;
                            break;
                        }
                    }
                    for (int j = 0; j < ThreePointValues.Length; j++)
                    {
                        if (lowerWord[i] == ThreePointValues[j])
                        {
                            //Console.WriteLine(word[i] + " checking for 3 points");
                            wordScore += 3;
                            charFound = true;
                            break;
                        }
                    }
                    for (int j = 0; j < FourPointValues.Length; j++)
                    {
                        if (lowerWord[i] == FourPointValues[j])
                        {
                            //Console.WriteLine(word[i] + " checking for 4 points");
                            wordScore += 4;
                            charFound = true;
                            break;
                        }
                    }
                    for (int j = 0; j < FivePointValues.Length; j++)
                    {
                        if (lowerWord[i] == FivePointValues[j])
                        {
                            //Console.WriteLine(word[i] + " checking for 5 points");
                            wordScore += 5;
                            charFound = true;
                            break;
                        }
                    }
                    for (int j = 0; j < EightPointValues.Length; j++)
                    {
                        if (lowerWord[i] == EightPointValues[j])
                        {
                            //Console.WriteLine(word[i] + " checking for 8 points");
                            wordScore += 8;
                            charFound = true;
                            break;
                        }
                    }
                    for (int j = 0; j < TenPointValues.Length; j++)
                    {
                        if (lowerWord[i] == TenPointValues[j])
                        {
                            //Console.WriteLine(word[i] + " checking for 10 points");
                            wordScore += 10;
                            charFound = true;
                            break;
                        }
                    }
                }
               

            }

            return wordScore;
        }

    }
}
