﻿using NUnit.Framework;
using code_challenge.Challenges;
using System;
namespace code_challenge
{
    class MainClass
    {
        
        public static void Main(string[] args)
        {
            var calculator = new Challenges.Calculator();
            var dnaParsing = new DnaParsing();
            var scrabbleScore = new Challenges.ScrabbleScoreCalculator();

            int add1 = 2;
            int add2 = 2;

            int sub1 = 4;
            int sub2 = 2;

            int multi1 = 2;
            int multi2 = 3;

            int div1 = 6;
            int div2 = 3;

            string DNA1 = "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC";
            string DNA2 = "AAGGTTCC";
            string word1 = "zoo";
            string word2 = "joke";

            Console.WriteLine(add1 + " + " + add2 + " = " + calculator.Add(add1, add2));
            Console.WriteLine(sub1 + " - " + sub2 + " = " + calculator.Subtract(sub1, sub2));
            Console.WriteLine(multi1 + " * " + multi2 + " = " + calculator.Multiply(multi1, multi2));
            Console.WriteLine(div1 + " / " + div2 + " = " + calculator.Divide(div1, div2));
            Console.WriteLine("DNA Test 1: " + dnaParsing.SequenceCount(DNA1));
            Console.WriteLine("DNA Test 2: " + dnaParsing.SequenceCount(DNA2));
            Console.WriteLine("Word score on word " + word1 +  ": " + scrabbleScore.WordScoreCheck(word1));
            Console.WriteLine("Word score on word " + word2 + ": " + scrabbleScore.WordScoreCheck(word2));
            Console.WriteLine("Hello World! Press enter to exit console: ");
            Console.ReadLine();
        }
    }
}
